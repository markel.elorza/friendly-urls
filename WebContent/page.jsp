<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Page</title>
		<style>
			table {
			  border-collapse: collapse;
			  margin: 1rem;
			}
			
			table, th, td {
			  border: 1px solid black;
			  padding: .25rem .5rem;
			}
		</style>
	</head>
	<body>
		<a href='${pageContext.request.contextPath}'>Go back</a><br/>
		<table>
			<thead>
				<tr><th>ID</th><th>Action</th></tr>
			</thead>
			<tbody>
				<tr>
					<td><c:out value="${requestScope.id}"/></td>
					<td><c:out value="${requestScope.action}"/></td>
				</tr>
			</tbody>
		</table>
	</body>
</html>