<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:bundle basename="resources.Resources">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><fmt:message key="string.page.title"/></title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/login.css">
</head>
<body>
<jsp:include page="templates/header.jsp"/>
<c:if test="${not empty sessionScope.error}">
	<p class="error"><c:out value="${sessionScope.error}"/></p>
	<c:remove var="error" scope="session" />
</c:if>
<c:if test="${not empty sessionScope.message}">
	<p class="message"><c:out value="${sessionScope.message}"/></p>
	<c:remove var="message" scope="session" />
</c:if>

<div class="content">
	<c:choose>
	<c:when test="${not empty sessionScope.user}">
		<h2><fmt:message key="string.hi"/> <c:out value="${sessionScope.user.username}"/>!</h2>
		<p><fmt:message key="string.logged"/></p>
		<ul>
			<li><fmt:message key="string.user.id"/>: <c:out value="${sessionScope.user.userId}" /></li>
			<li><fmt:message key="string.user.firstname"/>: <c:out value="${sessionScope.user.firstName}" /></li>
			<li><fmt:message key="string.user.secondname"/>: <c:out value="${sessionScope.user.secondName}" /></li>
			<li><fmt:message key="string.user.email"/>: <c:out value="${sessionScope.user.email}"/></li>
		</ul>
		<form action="${pageContext.request.contextPath}/Login/logout">
			<button type="submit"><fmt:message key="string.logout"/></button>
		</form>
	</c:when>
	<c:otherwise>
		<div class="form-container">
			<h2><fmt:message key="string.login"/></h2>
			<form action="${pageContext.request.contextPath}/Login/login" method="post">
			<div>
				<input	type="text"	name="username" required/>
				<label>
					<fmt:message key="string.username"/>:
				</label>
			</div>
			<div>
				<input type="password" name="password" required />
				<label><fmt:message key="string.password"/>:</label>
			</div>
			<button type="submit"><fmt:message key='string.login'/></button>
			</form>
		</div>
	</c:otherwise>
</c:choose>
</div>

</fmt:bundle>
<jsp:include page="templates/footer.jsp"/>