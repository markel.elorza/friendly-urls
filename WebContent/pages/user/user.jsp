<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:include page="../../templates/header.jsp"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><fmt:message key="string.title.details"/></title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/details.css">
</head>
<body>
<fmt:bundle basename="resources.Resources">
<div class="content">

	<h2><fmt:message key="string.title.details"/></h2>
	<ul>
		<li>
			<span class="label"><fmt:message key="string.user.firstname"/>: </span>${requestScope.user.firstName}
		</li>
		<li>
			<span class="label"><fmt:message key="string.user.secondname"/>: </span>${requestScope.user.secondName}
		</li>
		<li>
			<span class="label"><fmt:message key="string.user.email"/>: </span>${requestScope.user.email}
		</li>
		<li>
			<span class="label"><fmt:message key="string.user.username"/>: </span>${requestScope.user.username}
		</li>
	</ul>
	<a href="${pageContext.request.contextPath}/user/${requestScope.user.userId}/edit"><fmt:message key="string.action.edit"/></a>
	<a href="${pageContext.request.contextPath}/user/${user.userId}/delete"><fmt:message key="string.action.delete"/></a>
</div>
</fmt:bundle>
<jsp:include page="../../templates/footer.jsp"/>