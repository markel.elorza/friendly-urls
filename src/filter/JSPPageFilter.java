package filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;

/**
 * This class will filter all direct calls to JSP pages.
 * @author aperez
 *
 */
@WebFilter("/*")
public class JSPPageFilter implements Filter {

  public JSPPageFilter() {
  }

	public void destroy() {
	}
	
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
    System.out.println("doFilter function");
	  HttpServletRequest request = (HttpServletRequest) req;
    HttpServletResponse response = (HttpServletResponse) res;
    // HttpSession session = request.getSession(false);
    
    String requestPath = request.getRequestURI();
    System.out.println(requestPath);
    
    if(requestPath.endsWith(".jsp")) {
      System.out.println("JSP file is being called. Do not allow.");
      response.sendRedirect("error");
    }else {
      System.out.println("No JSP file, continue with the request.");
      chain.doFilter(req, res);
    }
	}

	public void init(FilterConfig fConfig) throws ServletException {}

}
