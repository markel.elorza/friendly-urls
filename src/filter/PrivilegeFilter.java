package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.user.model.User;
import helper.ControllerHelper;

/**
 * Servlet Filter implementation class PrivilegeFilter
 */
@WebFilter("/user/*")
public class PrivilegeFilter implements Filter {

    /**
     * Default constructor. 
     */
    public PrivilegeFilter() {
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		System.out.println("filter user privileges function");
		HttpServletRequest req = (HttpServletRequest) request;
	    HttpServletResponse res = (HttpServletResponse) response;
	    HttpSession session = req.getSession(true);
	    User loggedUser = (User) session.getAttribute("user");
	    String requestPath = req.getRequestURI();
	    System.out.println(requestPath);
	    ControllerHelper ch = new ControllerHelper(req);
	    int id = ch.getId();
	    String action = ch.getAction();
	    
	    if (requestPath.endsWith("Login") || requestPath.endsWith("error")) {
	    	System.out.println("Authorized, you can continue");
	    	chain.doFilter(req, res);
	    }else if (!action.equals("create") && loggedUser == null) {
	    	System.out.println("Trying to access unauthorized page. Do not allow.");
		    res.sendRedirect("error");
	    } else if((action.equals("edit") || action.equals("delete")) && id != loggedUser.getUserId()) {
	    	System.out.println("Trying to alter unauthorized User. Do not allow.");
		    res.sendRedirect("error");
	    } else {
	      System.out.println("Authorized, you can continue");
	      chain.doFilter(req, res);
	    }
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
