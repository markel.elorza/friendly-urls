package domain.user.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import config.MySQLConfig;
import domain.user.model.User;

public class DaoUserMySQL implements DaoUser {

	private MySQLConfig mysqlConfig;
	public DaoUserMySQL() {
		mysqlConfig = MySQLConfig.getInstance();
	}
	@Override
	public void insertUser(User user) {
		System.out.println("insertUser not implemented yet.");
	}

	@Override
	public User loadUser(String username, String password) {
    String sqlQuery = "SELECT * FROM user WHERE username=? AND password=?";
    User user = null;
    Connection connection = mysqlConfig.connect();
    PreparedStatement stm = null;
    try{
      stm = connection.prepareStatement(sqlQuery);
      stm.setString(1, username);
      stm.setString(2, password);
      System.out.println(stm);
      ResultSet rs = stm.executeQuery();
      if(rs.next()){
        user = new User();
        user.setUserId(rs.getInt("userId"));
        user.setUsername(rs.getString("username"));
        user.setPassword(rs.getString("password"));
        user.setFirstName(rs.getString("first_name"));
        user.setSecondName(rs.getString("second_name"));
        user.setEmail(rs.getString("email"));
      }
    }catch(SQLException e){
      e.printStackTrace();
      System.out.println("Error DaoLoginMysql loadUser "+username);
    }
    mysqlConfig.disconnect(connection, stm);
    return user;
	}

	@Override
	public User loadUser(int userId) {
		String sqlQuery = "SELECT * FROM user WHERE userId=?";
	    User user = null;
	    Connection connection = mysqlConfig.connect();
	    PreparedStatement stm = null;
	    try{
	      stm = connection.prepareStatement(sqlQuery);
	      stm.setInt(1, userId);
	      System.out.println(stm);
	      ResultSet rs = stm.executeQuery();
	      if(rs.next()){
	        user = new User();
	        user.setUserId(rs.getInt("userId"));
	        user.setUsername(rs.getString("username"));
	        user.setPassword(rs.getString("password"));
	        user.setFirstName(rs.getString("first_name"));
	        user.setSecondName(rs.getString("second_name"));
	        user.setEmail(rs.getString("email"));
	      }
	    }catch(SQLException e){
	      e.printStackTrace();
	      System.out.println("Error DaoUserMysql loadUser (byID) ");
	    }
	    mysqlConfig.disconnect(connection, stm);
	    return user;
	}

	@Override
	public ArrayList<User> loadUsers() {
		String sqlQuery = "SELECT * FROM user";
		User user = null;
	    Connection connection = mysqlConfig.connect();
	    PreparedStatement stm = null;
	    ArrayList<User> users = new ArrayList<>();
	    try {
	    	stm = connection.prepareStatement(sqlQuery);
	    	System.out.println(sqlQuery);
	    	ResultSet rs = stm.executeQuery();
	    	while (rs.next()) {
	    		user = new User();
	    		user = fillUser(user, rs);
	    		users.add(user);
	    	}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Error DaoUserMysql loadUsers");
		}
		return users;
	}
	
	private User fillUser(User user, ResultSet rs) {
		try {
			user.setUserId(rs.getInt("userId"));
			user.setUsername(rs.getString("username"));
			user.setPassword(rs.getString("password"));
			user.setFirstName(rs.getString("first_name"));
			user.setSecondName(rs.getString("second_name"));
			user.setEmail(rs.getString("email"));
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Error DaoUserMysql fillUsers ");
		}
		return user;
	}
	
	@Override
	public int createUser(User user) {
		String sqlQuery = "INSERT INTO user (first_name, second_name, email, username, password) " + 
				"VALUES (?, ?, ?, ?, ?); ";
		Connection connection = mysqlConfig.connect();
	    PreparedStatement stm = null;
	    int tmp = 0;
	    try{
	        stm = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
	        stm.setString(1, user.getFirstName());
	        stm.setString(2, user.getSecondName());
	        stm.setString(3, user.getEmail());
	        stm.setString(4, user.getUsername());
	        stm.setString(5, user.getPassword());
	      
	        stm.executeUpdate();
	        ResultSet rs= stm.getGeneratedKeys();
			
			if(rs.next()){
				tmp=rs.getInt(1);
			}
	      }catch(SQLException e){
	        e.printStackTrace();
	        System.out.println("Error DaoUserMysql create User ");
	        return -1;
	      }
		return tmp;
	}
	
	@Override
	public User editUser(User user) {
		String sqlQuery = "UPDATE user SET first_name = ?, second_name = ?, email = ?, username = ?, password = ? "
				+ "WHERE userId = ?";
		Connection connection = mysqlConfig.connect();
	    PreparedStatement stm = null;
	    try{
	        stm = connection.prepareStatement(sqlQuery);
	        stm.setString(1, user.getFirstName());
	        stm.setString(2, user.getSecondName());
	        stm.setString(3, user.getEmail());
	        stm.setString(4, user.getUsername());
	        stm.setString(5, user.getPassword());
	        stm.setInt(6, user.getUserId());
	        
	        stm.execute();
	      }catch(SQLException e){
	        e.printStackTrace();
	        System.out.println("Error DaoUserMysql create User ");
	        return null;
	      }
		return user;
	}
	
	@Override
	public void deleteUser(int userId) {
		String sqlQuery = "delete from user where userId = ?";
	    Connection connection = mysqlConfig.connect();
	    PreparedStatement stm = null;
	    try{
	      stm = connection.prepareStatement(sqlQuery);
	      stm.setInt(1, userId);
	      stm.execute();
	    }catch(SQLException e){
	      e.printStackTrace();
	      System.out.println("Error DaoUserMysql delete (byID) ");
	    }
	    mysqlConfig.disconnect(connection, stm);
		
	}

}
