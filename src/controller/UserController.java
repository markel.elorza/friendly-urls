package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.user.dao.UserFacade;
import domain.user.model.User;
import helper.ControllerHelper;

/**
 * Servlet implementation class UserController
 */
@WebServlet("/user/*")
public class UserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("User Controller doGet() function");
		ControllerHelper controllerHelper = new ControllerHelper(request);
	    int id = controllerHelper.getId();
	    String action = controllerHelper.getAction();
	    String requestedPage = "";
	    switch (action) {
			case "list":
				requestedPage = listUsers(request);
				break;
			case "create":
				requestedPage = createUsersForm(request);
				break;
			case "edit":
				requestedPage = editUser(request, id);
				break;
			case "view":
				requestedPage = viewUser(request, id);
				break;
			case "delete":
				requestedPage = deleteUser(request, id);
				break;
			case "error":
				requestedPage = errorPage();
				break;
			default:
				break;
		}
	    if (requestedPage.equals("delete")) {
	    	response.sendRedirect(request.getContextPath()+"/Login/logout");
	    }else {
	    	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(requestedPage);
	        dispatcher.forward(request, response);
	    }
	    
	}


	

	private String errorPage() {
		return "/error.jsp";
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ControllerHelper controllerHelper = new ControllerHelper(request);
	    String action = controllerHelper.getAction();
	    String requestedPage = "";
	    switch (action) {
			case "create":
				requestedPage = createUser(request);
				break;
			case "edit":
				requestedPage = editUser(request);
				break;
			default:
				break;
		}
	    
	    response.sendRedirect(requestedPage);
	}
	
	private String deleteUser(HttpServletRequest request, int userId) {
		UserFacade uf = new UserFacade();
		uf.deleteUser(userId);
		return "delete";
	}
	
	private String listUsers(HttpServletRequest request) {
		ArrayList<User> users = null;
		UserFacade uf = new UserFacade();
		users = uf.loadUsers();
		request.setAttribute("userList", users);
		request.setAttribute("currentPage", "list");
		return "/pages/user/user_list.jsp";
	}
	
	private String createUsersForm(HttpServletRequest request) {
		request.setAttribute("currentPage", "form");
		return "/pages/user/user_form.jsp";/*/pages*/
	}

	private String createUser(HttpServletRequest request) {
		UserFacade uf = new UserFacade();
		User user = new User();
		user = fillUser(user, request);
		
		int userId = uf.createUser(user);
		
		HttpSession session = request.getSession(false);
		if (session.getAttribute("user") == null ) return request.getContextPath();
		return request.getContextPath()+"/user/"+userId;
	}
	
	private String editUser(HttpServletRequest request, int userId) {
		UserFacade uf = new UserFacade();
		User user;
		user = uf.loadUser(userId);
		request.setAttribute("user", user);

		request.setAttribute("currentPage", "form");
		return "/pages/user/user_form.jsp";
	}
	
	private String viewUser(HttpServletRequest request, int userId) {
		UserFacade uf = new UserFacade();
		User user;
		user = uf.loadUser(userId);
		request.setAttribute("user", user);
		return "/pages/user/user.jsp";
	}
	
	private String editUser(HttpServletRequest request) {
		UserFacade uf = new UserFacade();
		User user = new User();
		user = fillUser(user, request);
		String id = request.getParameter("userId");
		user.setUserId(Integer.parseInt(id));
		System.out.println("User id: "+user.getUserId());
		
		uf.editUser(user);
		return request.getContextPath()+"/user/"+user.getUserId();
	}
	
	private User fillUser(User user , HttpServletRequest request) {
		user.setFirstName(request.getParameter("firstName"));
		user.setSecondName(request.getParameter("secondName"));
		user.setEmail(request.getParameter("email"));
		user.setUsername(request.getParameter("username"));
		user.setPassword(request.getParameter("password"));
		return user;
	}
}
