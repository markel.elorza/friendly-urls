package resources;
import java.util.ListResourceBundle;


public class Resources extends ListResourceBundle{
  private static final Object[][] contents = {
      {"string.lang.en", "English"},
      {"string.lang.es", "Spanish"},
      {"string.lang.eu", "Basque"},
      {"string.page.title", "User CRUD exercise"},
      {"string.menu.list", "List Users"},
      {"string.menu.create","Create User"},
      {"string.login","Login"},
      {"string.username","Username"},
      {"string.password","Password"},
      {"string.user.id","ID"},
      {"string.user.username","Username"},
      {"string.user.email","E-mail"},
      {"string.user.firstname","First Name"},
      {"string.user.secondname","Second Name"},
      {"string.user.password","Password"},
      {"string.action.edit","Edit"},
      {"string.action.delete","Delete"},
      {"string.hi", "Wellcome"},
      {"string.logged", "You are now logged in!"},
      {"string.logout", "Log out"},
      {"string.title.list", "List of Users"},
      {"string.title.form", "User Form"},
      {"string.title.create", "New User"},
      {"string.title.edit", "Edit User"},
      {"string.title.details", "User Details"},
      {"string.title.error", "Error page"},
      {"string.menu.home", "Home"},
      {"string.button.create", "Create"},
      {"string.button.edit", "Edit"}
  };
  
  @Override
  protected Object[][] getContents() {
    return contents;
  }

}
